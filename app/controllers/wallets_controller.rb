class WalletsController < ApplicationController
  before_action :load_wallet

  def deposit
    @wallet.deposit(wallet_params[:amount])
    render json: @wallet
  end

  private
  def load_wallet
    wallet_id = wallet_params[:id]
    @wallet = Wallet.where(id: wallet_id).take
  end

  def wallet_params
    params.permit(:id, :amount)
  end
end
