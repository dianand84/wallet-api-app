class Wallet < ApplicationRecord
  validates :balance, presence: true

  def deposit(amount)
    amount = amount.to_f
    update_attributes(balance: (balance + amount))
  end
end
