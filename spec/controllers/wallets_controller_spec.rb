require 'rails_helper'

describe WalletsController do
  describe 'POST /wallets/deposit' do
    let(:wallet) { FactoryGirl.create(:wallet, balance: 200) }

    it 'should deposit an amount and update balance accordingly' do
      post :deposit, params: {id: wallet.id, amount: "100"}
      expect(response).to be_ok
      actual = JSON.parse(response.body)
      expect(actual['balance']).to eq(300)
      expect(actual['id']).to eq(wallet.id)
    end
  end
end
