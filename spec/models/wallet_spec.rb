require 'rails_helper'

describe Wallet do
  it { should validate_presence_of(:balance) }

  describe '#deposit' do
    let(:wallet) { FactoryGirl.create(:wallet) }
    
    it 'should add an amount to the balance' do
      wallet.deposit(500)
      expect(wallet.balance).to eq(500)
    end
  end
end
